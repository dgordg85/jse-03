package ru.kozyrev.tm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {
    public static String getDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = formatter.format(date);
        return formattedDate;
    }
}
