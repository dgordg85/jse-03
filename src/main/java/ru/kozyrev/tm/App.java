package ru.kozyrev.tm;

import ru.kozyrev.tm.manager.CommandManager;
import ru.kozyrev.tm.enumerated.TerminalCommand;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        CommandManager cm = new CommandManager(sc);
        TerminalCommand command;
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            try {
                command = TerminalCommand.valueOf(sc.nextLine().toUpperCase().replace('-', '_'));
                if (command.equals(TerminalCommand.EXIT)) {
                    break;
                }
                cm.make(command);
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong command! Use 'help'!");
            }
        }
        sc.close();
    }
}
