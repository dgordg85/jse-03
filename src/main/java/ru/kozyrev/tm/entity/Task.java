package ru.kozyrev.tm.entity;


import ru.kozyrev.tm.util.DateFormatter;

import java.util.Date;
import java.util.UUID;

public class Task {
    private String name = "default name";
    private String id = UUID.randomUUID().toString();
    private String projectId = "";
    private String description = "";
    private Date dateStart = new Date();
    private Date dateFinish = new Date();

    public Task() {
    }

    public Task(String name, String projectId) {
        if (name != null)
            this.name = name;
        if (projectId != null)
            this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getBegin() {
        return DateFormatter.getDate(dateStart);
    }


    public String getEnd() {
        return DateFormatter.getDate(dateFinish);
    }
}
