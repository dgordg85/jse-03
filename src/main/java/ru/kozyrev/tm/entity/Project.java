package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.util.DateFormatter;

import java.util.Date;
import java.util.UUID;

public class Project {
    private String name = "default name";
    private String id = UUID.randomUUID().toString();
    private String description = "";
    private Date dateStart = new Date();
    private Date dateFinish = new Date();

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getBegin() {
        return DateFormatter.getDate(dateStart);
    }

    public String getEnd() {
        return DateFormatter.getDate(dateFinish);
    }
}
