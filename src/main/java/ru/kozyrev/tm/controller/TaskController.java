package ru.kozyrev.tm.controller;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;

import java.util.*;

public class TaskController {
    private List<Task> listTasks = new ArrayList<>();
    private ProjectController projectController;
    private Scanner sc;

    public TaskController(Scanner sc) {
        this.sc = sc;
    }

    public void setProjectController(ProjectController projectController) {
        this.projectController = projectController;
        //Демо данные
        this.projectController.getListProjects().add(new Project("Проект 1"));
        this.projectController.getListProjects().add(new Project("Проект 2"));
        this.projectController.getListProjects().add(new Project("Проект 3"));
        this.projectController.getListProjects().add(new Project("Проект 4"));
        this.projectController.getListProjects().add(new Project("Проект 5"));
        this.projectController.getListProjects().add(new Project("Проект 6"));
        listTasks.add(new Task("Пункт 1", projectController.getListProjects().get(0).getId()));
        listTasks.add(new Task("Пункт 2", projectController.getListProjects().get(0).getId()));
        listTasks.add(new Task("Пункт 3", projectController.getListProjects().get(0).getId()));
        listTasks.add(new Task("Пункт 4", projectController.getListProjects().get(0).getId()));
        listTasks.add(new Task("Пункт 5", projectController.getListProjects().get(0).getId()));
        listTasks.add(new Task("Пункт 1", projectController.getListProjects().get(1).getId()));
        listTasks.add(new Task("Пункт 2", projectController.getListProjects().get(1).getId()));
        listTasks.add(new Task("Пункт 3", projectController.getListProjects().get(1).getId()));
        listTasks.add(new Task("Пункт 1", projectController.getListProjects().get(2).getId()));
        listTasks.add(new Task("Пункт 2", projectController.getListProjects().get(2).getId()));
        listTasks.add(new Task("Пункт 3", projectController.getListProjects().get(2).getId()));
        listTasks.add(new Task("Пункт 4", projectController.getListProjects().get(2).getId()));
        listTasks.add(new Task("Пункт 5", projectController.getListProjects().get(2).getId()));
        listTasks.add(new Task("Пункт 6", projectController.getListProjects().get(2).getId()));
    }

    public void create() {
        System.out.println("[TASK CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        if (name.length() == 0) {
            name = null;
        }
        System.out.println("ENTER PROJECT ID:");
        try {
            String id = sc.nextLine();
            if (id.length() != 0) {
                int projectId = Integer.parseInt(id);
                id = projectController.getListProjects().get(projectId).getId();
            } else {
                id = null;
            }
            listTasks.add(new Task(name, id));
            System.out.println("[OK]");
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void update() {
        System.out.println("[UPDATE TASK]\nENTER ID:");
        try {
            int taskId = Integer.parseInt(sc.nextLine());
            Task task = listTasks.get(taskId);
            System.out.println("NEW NAME:" + " (default: " + task.getName() + ")");
            String name = sc.nextLine();
            if (name.length() != 0) {
                task.setName(name);
                System.out.println("[OK]");
            }
            System.out.println("NEW PROJECT ID:");
            String id = sc.nextLine();
            if (id.length() != 0) {
                setProjectId(task, Integer.parseInt(id));
                System.out.println("[OK]");
            }
            printList(task.getProjectId());

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void getList() {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectId = sc.nextLine();
        if (projectId.length() == 0) {
            printList();
        } else {
            try {
                printList(Integer.parseInt(projectId));
            } catch (NumberFormatException e) {
                System.out.println("Wrong index!");
            }
        }
    }

    public void printList() {
        System.out.println("[TASKS LIST]");
        for (int i = 0; i < listTasks.size(); i++) {
            System.out.printf("%d. %s, ProjectID# %s, ID# %s\n", i, listTasks.get(i).getName(), listTasks.get(i).getProjectId(), listTasks.get(i).getId());
        }
    }

    public void printList(int projectId) {
        printList(projectController.getListProjects().get(projectId).getId());
    }

    public void printList(String projectUUID) {
        System.out.printf("[TASKS LIST OF PROJECT '%s']\n", projectController.getProjectByUUID(projectUUID).getName());
        for (int i = 0; i < listTasks.size(); i++) {
            if (listTasks.get(i).getProjectId().equals(projectUUID)) {
                System.out.printf("%d. %s, ID# %s\n", i, listTasks.get(i).getName(), listTasks.get(i).getId());
            }
        }
    }

    public void remove() {
        System.out.println("[TASK DELETE]\nENTER ID:");
        try {
            int id = Integer.parseInt(sc.nextLine());
            String projectUUID = listTasks.get(id).getProjectId();
            listTasks.remove(id);
            printList(projectUUID);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void clear() {
        System.out.println("[CLEAR]");
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT]");
        String id = sc.nextLine();
        if (id.length() == 0) {
            clearAll();
        } else {
            try {
                clearProjectTasks(Integer.parseInt(id));
            } catch (NumberFormatException e) {
                System.out.println("Wrong index!");
            }
        }
    }

    public void clearAll() {
        listTasks.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void clearProjectTasks(int projectId) {
        String projectUUID = projectController.getListProjects().get(projectId).getId();
        clearProjectTasks(projectUUID);
    }

    public void clearProjectTasks(String projectUUID) {
        Iterator<Task> iterator = listTasks.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getProjectId().equals(projectUUID)) {
                iterator.remove();
            }
        }
        System.out.println("[TASKS OF PROJECT JUST REMOVED]");
    }

    public void setProjectId(Task task, int projectId) {
        try {
            String projectUUID = projectController.getListProjects().get(projectId).getId();
            task.setProjectId(projectUUID);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }
}
