package ru.kozyrev.tm.controller;

import ru.kozyrev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectController {
    private List<Project> listProjects = new ArrayList<>();
    private TaskController taskController;
    private Scanner sc;

    public ProjectController(Scanner sc) {
        this.sc = sc;
    }

    public void setTaskController(TaskController taskController) {
        this.taskController = taskController;
    }

    public void create() {
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        if (name.length() != 0) {
            listProjects.add(new Project(name));
        } else {
            listProjects.add(new Project());
        }
        printList();
    }

    public void remove() {
        try {
            System.out.println("[PROJECT REMOVE]\nENTER ID:");
            int id = Integer.parseInt(sc.nextLine());
            String projectUUID = listProjects.get(id).getId();
            listProjects.remove(id);
            System.out.printf("[PROJECT ID=%d REMOVED]\n", id);
            taskController.clearProjectTasks(projectUUID);
            printList();
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void update() {
        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        try {
            int id = Integer.parseInt(sc.nextLine());
            Project project = listProjects.get(id);
            System.out.println("[UPDATE PROJECT]\nENTER NEW NAME:");
            String name = sc.nextLine();
            if (name.length() != 0) {
                project.setName(name);
                printList();
            } else {
                System.out.println("Name is empty!");
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void getList() {
        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        try {
            String projectId = sc.nextLine();
            if (projectId.length() == 0) {
                printList();
            } else {
                printList(Integer.parseInt(projectId));
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println("Wrong index!");
        }
    }

    public void printList() {
        System.out.println("[PROJECTS LIST]");
        for (int i = 0; i < listProjects.size(); i++) {
            System.out.printf("%d. %s, ID# %s\n", i, listProjects.get(i).getName(), listProjects.get(i).getId().toString());
        }
    }

    public void printList(int projectId) {
        taskController.printList(projectId);
    }

    public void clear() {
        System.out.println("[CLEAR]");
        listProjects.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
        taskController.clearAll();
    }

    public List<Project> getListProjects() {
        return listProjects;
    }

    public Project getProjectByUUID(String id) {
        for (int i = 0; i < listProjects.size(); i++) {
            if (listProjects.get(i).getId().equals(id)) {
                return listProjects.get(i);
            }
        }
        return null;
    }
}
