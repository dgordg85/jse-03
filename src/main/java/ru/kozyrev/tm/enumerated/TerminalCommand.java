package ru.kozyrev.tm.enumerated;

public enum TerminalCommand {
    TASK_LIST("task-list", "Show all tasks."),
    TASK_CREATE("task-create", "Create new tasks."),
    TASK_UPDATE("task-update", "Update selected task."),
    TASK_REMOVE("task-remove", "Remove selected task."),
    TASK_CLEAR("task-clear", "Remove all tasks."),
    HELP("help", "Show all commands."),
    EXIT("exit", "Quit from manager."),
    PROJECT_LIST("project-list", "Show all projects."),
    PROJECT_CREATE("project-create", "Create new project."),
    PROJECT_UPDATE("project-update", "Update Selected project."),
    PROJECT_REMOVE("project-remove", "Remove Selected project."),
    PROJECT_CLEAR("project-clear", "Remove all projects.");

    String title;
    String value;

    TerminalCommand(String title, String value) {
        this.title = title;
        this.value = value;
    }

    @Override
    public String toString() {
        return title;
    }

    public static void printAlCommands() {
        for (TerminalCommand c : TerminalCommand.values()) {
            System.out.println(c.title + ": " + c.value);
        }

    }
}
